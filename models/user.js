/** 
*  User model
*  Describes the characteristics of each attribute in a user resource.
*
* @author Ravi Kumar Atluri <s534734@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  accountNumber: { type: Number, required: true },
  contact: { type: String, required: true, default: ''  },
  email: { type: String, required: true, unique: true, default: ''  },
  firstname: { type: String, required: true, default: '' },
  family: { type: String, required: true, default: '' },
  gender: {type: String, required: false, default: ''},
  street1: { type: String, required: true, default: '' },
  street2: { type: String, required: false, default: '' },
  city: { type: String, required: false, default: '' },
  state: { type: String, required: true, default: 'MO' },
  zip: { type: String, required: true, default: '64468' },
  country: { type: String, required: true, default: '' }
})

module.exports = mongoose.model('User', UserSchema)
