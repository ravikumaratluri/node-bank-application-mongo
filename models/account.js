/** 
*  Account model
*  Describes the characteristics of each attribute in a Account resource.
*
* @author Varun Goud Pulipalpula <s534848@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const accountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  accountnumber:{type:Number,required: true, default: ''},
  accountName : {type:String,required: true, default: ''},
  userId:{type:Number,required: true, default: ''},
  transactionId:{type: Number, required: true, default: ''},
})

module.exports = mongoose.model('account', accountSchema)
